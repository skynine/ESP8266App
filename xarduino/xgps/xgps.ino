#include <Arduino.h>
#include "SoftSerial.h"
#include "Wire.h"  //声明I2C库文件
extern "C" {
#include "utility/twi.h"  // from Wire library, so we can do bus scanning
}

SoftSerial gpsSerial; // RX-8, TX-9
SoftSerial wifiSerial; // RX-10, TX-11

#define LED 13
byte x = 0;//变量x决定LED的亮灭

char agps='\r';
String scall;

void setup() {
    Serial.println("WiFi GPS by openthings@163.com.");

    Wire.begin(); // 加入 i2c 总线，作为主机
    pinMode(LED,OUTPUT);//设置数字端口13为输出
    
    Serial.begin(9600);
    Serial.println("Initialize Serial @9600bps.");
    //wifi.init(9600,10,11);
    
    gpsSerial.init(8,9);
    gpsSerial.begin(9600);
    Serial.println("Initialize GPS @9600bps.");
    
    wifiSerial.init(10,11);
    wifiSerial.begin(9600);
    Serial.println("Initialize WiFi @9600bps.");
    //wifiSerial.listen();
    gpsSerial.listen();
    Serial.println("WiFi GPS ready.");
}

void loop() {
  // put your main code here, to run repeatedly:
  //test();
  //wifibridge();
  gps2wifi();
}

void test()
{
  wifiSerial.println("hello,from arduino.");
  delay(1000);
}

//建立硬串口（USB）与软件串口的透传通道。
//模块调试和测试是非常有用哦！
//Transform between UART and SoftSerial.
void wifibridge() 
{
    if (wifiSerial.available()) {
        Serial.write(wifiSerial.read());
    }
    if (Serial.available()) {
        wifiSerial.write(Serial.read());
    }
}

void gpsbridge()
{
    char agps='\r';
    if (gpsSerial.available()) {  
       agps = gpsSerial.read();
       if (agps == 'u') {
       } else {
         Serial.write(agps);
       }
    }
    
    if (Serial.available()) {
        gpsSerial.write(Serial.read());
    }
}

void gps2wifi() 
{
    if (gpsSerial.available()) {
       agps = gpsSerial.read();
       Serial.write(agps);
       //send2I2C(agps);
       if (agps == '\n') {
         delay(2000);
       } else {
         wifiSerial.write(agps);
       }
       //scall += String("userver.ondata(\"") + agps + String("\")");
       //Serial.println(scall);
       //wifiSerial.write(scall);
    }

    if (wifiSerial.available()) {
        Serial.write(wifiSerial.read());
    }    
    if (Serial.available()) {
        wifiSerial.write(Serial.read());
    }
}

void send2I2C(char cd)
{
    Wire.beginTransmission(4); //发送数据到设备号为4的从机
    //Wire.write("light is ");        // 发送字符串"light is "
    Wire.write(cd);              // 发送变量x中的一个字节  
    Wire.endTransmission();    // 停止发送
}

void recvI2C()
{
    Wire.requestFrom(4, 1);    //通知4号从机上传1个字节
    while(Wire.available())    // 当主机接收到从机数据时
    { 
      byte c = Wire.read(); //接收一个字节赋值给c
      
      //判断c为1，则点亮LED，否则熄灭LED。
      //if(c==1) {
      //  digitalWrite(LED,LOW);
      //} else {
      //  digitalWrite(LED,HIGH);
      //}
    }
}

void scanI2CBus(byte from_addr, byte to_addr, 
                void(*callback)(byte address, byte result) ) 
{
  byte rc;
  byte data = 0; // not used, just an address to feed to twi_writeTo()
  //for( byte addr = from_addr; addr <= to_addr; addr++ ) {
  //  rc = twi_writeTo(addr, &data, 0, 1);
  //  if(rc==0) callback( addr, rc );
  //}
}

void scanFunc( byte addr, byte result ) 
{
  Serial.print("addr: ");
  Serial.print(addr,DEC);
  addr = addr<<1;
  Serial.print("\t HEX: 0x");
  Serial.print(addr,HEX);
  Serial.println( (result==0) ? "\t found!":"   ");
  //Serial.print( (addr%4) ? "\t":"\n");
}




