#!/bin/bash

echo "================================="
echo "Build all for ESP8266."
echo "================================="

cd ./build

./build_mpy.sh
./build_mqtt.sh
./build_nlua.sh
./build_nmcu.sh
./build_rtos.sh
./build_xcmd.sh
./build_frank.sh

cd ..
echo "================================="
echo "Build all for ESP8266 is OK."
echo "Plase check firmware."
echo "================================="


