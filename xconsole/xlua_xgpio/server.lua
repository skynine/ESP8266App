local xserver = {}

function xserver.tohtml(conn,str)
    conn:send("HTTP/1.1 200 OK\n\n")
    conn:send("<html><body>")
    conn:send(str.."<BR>")
    conn:send("</html></body>")
end

function xserver.onlisten(conn)
	print("client connected.")
 	xt=package.loaded["xgpio"]
 	if (xt~=nil) then
		xserver.tohtml(conn,"switch")
	else
		conn:send("xgpio invalid.")
	end
	conn:on("sent",function(conn) conn:close() end)
end

function xserver.start()
	print("t_server start listen.IP:"..wifi.sta.getip()..":8080,HEAP:"..node.heap())
	srv=net.createServer(net.TCP)
	srv:listen(8080,xserver.onlisten)
end

return xserver
