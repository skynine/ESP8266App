#!/usr/bin/env python
#This is a x file for nodemcu, maked by ZeroDay.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-10-15.

'''
例子：
	pin=1
	gpio.mode(pin, gpio.OUTPUT)
	gpio.write(pin, gpio.HIGH)
将索引1的pin设置为GPIO模式，并设置为高电平。
'''

#描述：设置对应pin的输入输出模式，将该pin初始化为gpio模式。
#pin：0~11，IO索引编号
#mode：gpio.OUTPUT或者gpio.INPUT
def gpio_mode( pin, mode):
	strResult = x_serial_xcmd("gpio.mode("+pin+","+mode+")");
	return strResult
	
#描述：读取对应pin的值
#pin：0~11，IO索引编号
#返回：0表示低，1表示高
def gpio_read(pin):
	strResult = x_serial_xcmd("gpio.read("+pin+")");
	return strResult

#描述：设置对应pin的值
#pin：0~11，IO索引编号
#level：gpio.HIGH或者gpio.LOW
def def gpio_write(pin, level):
	strResult = x_serial_xcmd("gpio.write("+pin+","+level+")");
	return strResult



