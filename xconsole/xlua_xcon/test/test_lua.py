#!/usr/bin/env python
#This is a test file for nodemcu, maked by ZeroDay.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-10-15.

import serial

def lua_sayhello(serial ser):
	ser.write("print(\",hello.\"+")")
	print ser.readline()

def lua_loop(serial ser):
	for i in range(0,3):
		ser.write("print("+i+",hello."+")")
		print ser.readline()



