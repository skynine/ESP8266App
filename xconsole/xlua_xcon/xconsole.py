#!/usr/bin/env python
#coding=utf-8

#This is a x file for nodemcu, maked by ZeroDay.
#Using this to automate the discovery procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-10-18.

import x_serial

print("  ==================================================")
print("  * N    N  OO  DDDDD  EEEE   M     M  CCCC U    U *")
print("  * N N  N O  O  D  D E  E    M M M M C     U    U *")
print("  * N  N N O  O  D  D E E     M  M  M C     U    U *")
print("  * N    N  OO  DDDDD  EEEE   M     M  CCCC  UUUU  *")
print("  *------------------------------------------------*")
print("  *          Welcome to xConsole!                  *")
print("  *      For nodemcu based esp8266 SoC.            *")
print("  *     By openthings@163.com. 2014-10.            *")
print("  *------------------------------------------------*")
x_serial.isSimulate = 0
x_serial.port = '/dev/ttyUSB0'
x_serial.bandrate = 9600
x_serial.serialopen()
print "   Open Serial at: ",x_serial.port,",",x_serial.baudrate
print("   Now input Lua,@xxx for Host cmd,exit for EXIT. ")
print("  *================================================*")

#============================================
def exeSerial(strCMD):
	#print "EXEC: ",strCMD
	if (strCMD.find("@")==0):	#主机命令。
		strResult = exeHost(strCMD)
		return strResult
	else:
		strResult = x_serial.xcmd_all(strCMD)
		return strResult

#执行主机的操作。
def exeHost(strCMD):
	print "Host CMD: ",strCMD
	return "Local:",strCMD
		
#======================================
while(1):
	xcmd = raw_input("X:")
	if xcmd.upper()=="EXIT":
		break
	print exeSerial(xcmd)

x_serial.serialclose()	
print "xconsole Closed."
#======================================

