#!/usr/bin/env python
#coding=utf-8

#This file for XCMD, created by OpenThings.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-11-03.

import x_serial

#描述：设置wifi的工作模式
#mode：取值：wifi.STATION, wifi.SOFTAP或wifi.STATIONAP
#返回设置之后的当前mode
def setmode(mode):
	strResult = x_serial.xcmd("wifi.setmode(mode)")
	return strResult

#描述：获取wifi的工作模式
#返回：wifi.STATION, wifi.SOFTAP或wifi.STATIONAP
def getmode():
	strResult = x_serial.xcmd("wifi.getmode()")
	return strResult
	
#描述：开始智能配置，若成功自动设置ssid和pwd并退出
#channel：1~13，起始搜索信道。若不指定，默认为6，每个信道搜索20秒
#succeed_callback：配置成功后的回调函数，将在获取密码正确并连接上ap之后调用
def startconfig():
	strResult = x_serial.xcmd("wifi.startconfig( channel, function succeed_callback)")
	return strResult

#描述：中断智能配置
def stopconfig():
	strResult = x_serial_xcmd("wifi.stopconfig()");
	return strResult

#======================
#wifi.station module
#-----------------

#描述：设置station模式下的ssid和password
#ssid：字符串形式，长度小于32
#password：字符串形式，长度小于64
def apconfig(ssid,pwd)
	strResult = x_serial_xcmd("wifi.station.setconfig(\""+ssid+"\",\""+password+"\")")
	return strResult

#描述：station模式下连接至ap
def connect():
	strResult = x_serial_xcmd("wifi.station.connect()");
	return strResult


#描述：station模式下断开与ap的连接
def connect():
	strResult = x_serial_xcmd("wifi.station.disconnect()");
	return strResult

#描述：station模式下设置自动连接至ap
#auto：0表示设置为不自动连，1表示设置为自动连接
def autoconnect():
	strResult = x_serial_xcmd("wifi.station.autoconnect(auto)");
	return strResult

#描述：station模式下获取ip
#返回：字符串形式的ip，如"192.168.0.2"
def getip():
	strResult = x_serial_xcmd("wifi.station.getip()");
	return strResult

#描述：station模式下获取mac
#返回：字符串形式的mac，如"18-33-44-FE-55-BB"
def getmac():
	strResult = x_serial_xcmd("wifi.station.getmac()");
	return strResult

#==================================
#wifi.ap module
#---------------

#描述：设置station模式下的ssid和password
'''
cfg：设置需要的map
例子：
	cfg={}
	cfg.ssid="myssid"
	cfg.pwd="mypwd"
	wifi.ap.setconfig(cfg)
'''
def apsconfig():
	wifi.ap.setconfig(cfg)

#描述：ap模式下获取ip
#返回：字符串形式的ip，如"192.168.0.2"
def getapip():
	strResult = x_serial_xcmd("wifi.ap.getip()");
	return strResult

#描述：ap模式下获取mac
#返回：字符串形式的mac，如"1A-33-44-FE-55-BB"
def getapmac():
	strResult = x_serial_xcmd("wifi.ap.getmac()");
	return strResult

#描述：建立一个服务器
#type：net.TCP或 net.UDP
#返回：net.server子模块
def createserver(servertype):
	strResult = x_serial_xcmd("net.createServer("+servertype+")");
	return strResult
	
#描述：建立一个客户端
#type：net.TCP或 net.UDP
#返回：net.socket子模块
def createconnection():
	strResult = x_serial_xcmd("net.createConnection()")
	return strResult
	
#============================
#net.server module
#-------------

#描述：监听某端口
#port：端口号
#ip：可忽略，ip字符串
'''
例子：
	sv=net.createServer(net.TCP, false)
	sv:listen(80)
'''
def createconnection(port,ip):
	strResult = x_serial_xcmd("listen("+port+","+ip+")")
	return strResult

#描述：向连接的客户端发送数据
#string：需要发送的数据字符串
def send( string, function_sent ):
	strResult = x_serial_xcmd("send("+string+","+function_sent +")")
	return strResult

'''
例子：
	sv=net.createServer(net.TCP, false)
	sv:listen(80)
	sv:on("receive", function(s,c) s:send("Hello, world.") print(c) end )
'''

#描述：关闭服务器
def close():
	strResult = x_serial_xcmd("close()")
	return strResult
	

#描述：注册事件的回调函数
#event：字符串，可为："connection"，"reconnection"，"disconnection"，"receive"，"sent"
#function cb(net.server sv, [string])：回调函数。第一个参数为服务器本身。
#若event为"receive"， 第二个参数为接收到数据，字符串形式。
def on(event, function_cb):
	strResult = x_serial_xcmd("on("+event+","+function_cb +")")
	return strResult

#==========================
#net.socket module
#-------

#描述：连接到某ip和端口
#port：端口号
#ip：ip字符串
def socket_connect(port, ip):
	strResult = x_serial_xcmd("connect("+port+","+ip+")")
	return strResult


#描述：向连接发送数据
#string：需要发送的数据字符串
def socket_send(string, function_sent):
	strResult = x_serial_xcmd("send("+string+","+function_sent+")")
	return strResult

#描述：注册事件的回调函数
#event：字符串，可为："connection"，"reconnection"，"disconnection"，"receive"，"sent"
#function cb(net.socket, [string])：回调函数。第一个参数为socket连接本身。
#若event为"receive"， 第二个参数为接收到数据，字符串形式。
'''
例子：
	sk=net.createConnection(net.TCP, false)
	sk:on("receive", function(sck, c) print(c) end )
	sk:connect(80,"192.168.0.66") 
	sk:send("GET / HTTP/1.1\r\nHost: 192.168.0.66\r\nConnection: keep-alive\r\nAccept: */*\r\n\r\n")
'''
def socket_on(event, function_cb):
	strResult = x_serial_xcmd("on("+event+","+function_cb +")")
	return strResult

#描述：关闭socket
def sockekt_close():
	strResult = x_serial_xcmd("close()")
	return strResult


