local xgpio = {}

pin = 9
pinStat = gpio.HIGH
gpio.mode(pin,gpio.OUTPUT)

pinb =8
pinStatb = gpio.HIGH
gpio.mode(pinb,gpio.OUTPUT)

function xgpio.on()
	pinStat = gpio.LOW
	gpio.write(pin,gpio.LOW)
end

function xgpio.off()
	pinStat = gpio.HIGH
	gpio.write(pin,gpio.HIGH)
end

function xgpio.turn()
	if (pinStat==gpio.HIGH) then xgpio.on()
	else xgpio.off()
	end
end

return xgpio

