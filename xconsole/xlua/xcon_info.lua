
local xinfo = {}

function xinfo.about()
  print("LUA examples for nodemcu.")
  print("Author: openthings@163.com. copyright&GPL V2.")
  print("Last modified 2014-12-01.")
end

function xinfo.info()
	print("ChipID:"..node.chipid())
	print("Heap:"..node.heap())
	print("Mac:"..wifi.sta.getmac())
	print("Mac AP:"..wifi.ap.getmac())
	print("IP:"..wifi.sta.getip())
	print("IP of AP:"..wifi.ap.getip())	
end

return xinfo

