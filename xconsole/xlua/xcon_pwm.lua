--LUA examples for nodemcu.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-15.

-- D0 is connected to green led
-- D1 is connected to blue led
-- D2 is connected to red led
pwm.setup(0,500,50)
pwm.setup(1,500,50)
pwm.setup(2,500,50)
pwm.start(0)
pwm.start(1)
pwm.start(2)

function led(r,g,b)
  pwm.setduty(0,g)
  pwm.setduty(1,b)
  pwm.setduty(2,r)
end

led(50,0,0) --  set led to red
led(0,0,50) -- set led to blue.
