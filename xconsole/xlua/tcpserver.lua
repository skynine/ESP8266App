print("LUA examples for nodemcu.")
print("Author: openthings@163.com. copyright&GPL V2.")
print("Last modified 2014-11-15.")
print("Starting TCP Server at 8080...")

print(wifi.sta.getip())

-- create a server
sv=net.createServer(net.TCP, false)

-- server listen on 80, if data received, print data to console, and send "hello world" to remote.
sv:listen(8080,
	function(c)
		c:on("receive", 
			function(sck, pl)
				print(pl) 
				--c:send("RECEIVED:")
				--c:send(pl)
			end
		)
		c:send("RECEIVED:")
		--c:send(pl)
	end
)

print("TCPServer running at :8080")

