--LUA examples for nodemcu.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-15.

function led(r,g,b)
    pwm.setduty(0,r)
    pwm.setduty(1,g)
    pwm.setduty(2,b)
end

pwm.setup(0,500,50)
pwm.setup(1,500,50)
pwm.setup(2,500,50)

pwm.start(0)
pwm.start(1)
pwm.start(2)

led(50,0,0) -- red
led(0,0,50) -- blue
