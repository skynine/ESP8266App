将生成的固件刷新到芯片中：

1、合并固件，使用app.make.sh，生成app.wixserver.bin。
2、烧写固件，模块重新加电，运行app.flash.sh，将app.wixserver.bin写入SoC中。
3、模块重新加电。运行minicom或其它的串口工具。
4、输入@，如果返回OK，则刷新成功。

在linux虚拟机中，拔出模块前先行去选端口。
否则下次连入端口号自动增加,导致无法连接到原来的端口号。

