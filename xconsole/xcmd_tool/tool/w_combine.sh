#!/bin/bash

echo "Combine 0x00000 and 0x40000 into wixserver..."
python esptool.py make_image \
	-f ../firmware/0x00000.bin -a 0x00000 \
	-f ../firmware/0x40000.bin -a 0x40000 \
	wixserver.bin
echo "OK"


