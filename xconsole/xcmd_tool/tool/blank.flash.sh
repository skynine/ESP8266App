#!/bin/bash

echo "Upload blank512k to ESP8266..." 
echo "Don't forget set GPIO0 to LOW."

echo "writing firmware/blank512k.bin to 0x00000"
sudo python esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./blank512k.bin

echo "Finished."
echo "Don't forget unset GPIO0 to HIGH."

