#!/bin/bash
#https://github.com/themadinventor/esptool/blob/master/esptool.py

echo "Upload firmware to ESP8266..." 
echo "Don't forget set GPIO0 to LOW."

echo "writing firmware/0x00000.bin"
python esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ../firmware/0x00000.bin

echo "Finished."
echo "Don't forget unset GPIO0 to HIGH."

