local xgpio = {}

pin = 9
pinStat = gpio.HIGH
gpio.mode(pin,gpio.OUTPUT)

function xgpio.on()
	pinStat = gpio.LOW
	gpio.write(pin,gpio.LOW)
end

function xgpio.off()
	pinStat = gpio.HIGH
	gpio.write(pin,gpio.HIGH)
end

function xgpio.turn()
	if (pinStat==gpio.HIGH) then xgpio.on()
	else xgpio.off()
	end
end

function xgpio.setpin(pin,status)
	print("GPIO"..pin..":"..status)
	if (status==0) then gpio.write(pin,gpio.HIGH)
	else gpio.write(pin,gpio.LOW)
	end
end

function xgpio.getpin(pin)
	pin_status = gpio.read(pin)
	print("GPIO"..pin..":"..pin_status)
end

return xgpio

