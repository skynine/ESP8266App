#!/usr/bin/env python 
#Server for twisted.

import threading

from twisted.internet import protocol,reactor 
from time import sleep,ctime

HOST = '192.168.199.179'
PORT = 8889
BUFSIZ = 1024 
ADDR = (HOST, PORT) 

class twisProtocol(protocol.Protocol):
	def connectionMade(self):
		clnt = self.clnt = self.transport.getPeer().host
		print 'Connected from:',clnt
				
	def dataReceived(self,data):
		self.transport.write('[%s] %s' % (ctime(),data))
		print ctime(),"Received >>",data

class twisFactory(protocol.Protocol):
	protocol = twisProtocol
	clientConnectionLost = clientConnectionFailed = \
	lambda self,connector,reason:reactor.stop()
	

if __name__ == "__main__":
	factory = protocol.Factory()
	factory.protocol = twisProtocol
	
	print "Waiting for connect..."
	reactor.listenTCP(PORT,factory)
	reactor.run()
	

