#!/usr/bin/env python 
import threading
from twisted.internet import protocol,reactor 
from time import sleep,ctime

HOST = '192.168.199.227'
PORT = 8888
BUFSIZ = 1024
ADDR = (HOST, PORT) 

class twisProtocol(protocol.Protocol):
	def sendData(self):
		indata = raw_input('>')
		if indata:
			print 'Sending %s...'%indata
			self.transport.write(indata)
		else:
			self.transport.loseConnection()
	
	def connectionMade(self):
		print "Connected."
		self.sendData()
				
	def dataReceived(self,data):
		print data
		#print ctime(),">>","I am socket."

		self.sendData()
		
class twisFactory(protocol.ClientFactory):
	protocol = twisProtocol
	clientConnectionLost = clientConnectionFailed = \
	lambda self,connector,reason:reactor.stop()
	

if __name__ == "__main__":
	print "Connect to tcpserver..."
	reactor.connectTCP(HOST,PORT,twisFactory())
	reactor.run()
	

