#====================================================
#IP相关的设置。
#包括客户端和AP的IP地址，连接信息，建立到服务器端的连接等。

#查询IP地址（STATION模式）。
@+CIP?

#设置IP地址（STATION模式）。
@+CIP=*.*.*.*

#查询softAP的IP地址（AP/STA+AP模式）。
@+CIPAP?

#设置softAP的IP地址（AP/STA+AP模式）。
@+CIPAP=*.*.*.*
