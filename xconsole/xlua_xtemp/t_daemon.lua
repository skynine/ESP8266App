local xdaemon = {}

--who="ESP8266_TSERVER"
--isapconfig=1
--o=1

function xdaemon.oncallback()
	xt=package.loaded["t_ds18b20"]
	if xt==nil then
		xt=require("t_ds18b20")
		if xt==nil then 
			print("t_ds18b20 load fail. HEAP:"..node.heap())
		else
			print("t_ds18b20 loaded. HEAP:"..node.heap())
		end
	else
		xt.printc()
		--if o==1 then xt.printc() end	
	end
	
	if package.loaded["t_server"]==nil then
		xs=require("t_server")
		if xs==nil then
			print("t_server load fail. HEAP:"..node.heap())
		else 
			xs.start() 
			print("t_server loaded. HEAP:"..node.heap())
		end
	end
end

function xdaemon.oncheck()
	print("oncheck...")
	if wifi.sta.getip()==nil then
			print("config ap...")
			tmr.stop(0)
			apclib=require("t_apconfig")
			if apclib==nil then
				print("APConfig loaded failed. ")
			else
				print("APConfig Started. ")
				apclib.startconfig()
			end
	else
		if package.loaded["t_apconfig"]~=nil then 
			print("apconfig exist,restart...")
			node.restart()
		end
		xdaemon.oncallback()
	end
end

function xdaemon.start()
	print("start Daemon... HEAP:"..node.heap())
	tmr.alarm(0,5000, 1, xdaemon.oncheck)
end

return xdaemon
