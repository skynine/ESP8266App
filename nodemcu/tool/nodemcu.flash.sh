#!/bin/bash

echo "Upload firmware nodemcu to ESP8266..." 
echo "Don't forget set GPIO0 to LOW."

echo "writing firmware/0x00000.bin"
sudo python esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./nodemcu_512k.bin

echo "Finished."
echo "Don't forget unset GPIO0 to HIGH."


