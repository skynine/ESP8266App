#!/bin/bash

echo "Upload firmware nodemcu to ESP8266..." 

echo "Git pull, get latest version."
cd ../nodemcu/nodemcu-firmware/
git pull

echo "Copy nodemcu_512k.bin to this directory from git."
cd ../../tool
cp --force ../nodemcu/nodemcu-firmware/pre_build/0.9.4/512k-flash/nodemcu_512k_20141222.bin ./nodemcu_512k.bin

echo "Don't forget set GPIO0 to LOW."
echo "Strick any key to flash firmware."
read -n1 var

echo "writing nodemcu_512k.bin to Soc at 0x00000."
sudo python esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./nodemcu_512k.bin

echo "Finished."
echo "Don't forget unset GPIO0 to HIGH."

