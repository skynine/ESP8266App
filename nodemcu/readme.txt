/////////////////////////////////////////////
nodemcu, Using LUA develop somethings.
nodemcu, now based ESP8266 SoC.
nodemcu，脚本编程、自带WiFi的微控制器。
/////////////////////////////////////////////

download nodemcu firmware from:
>>https://github.com/nodemcu/nodemcu-firmware
or:
git clone https://github.com/nodemcu/nodemcu-firmware.git

Note:
nodemcu_firmware，支持lua的固件，需预先烧入SoC中。
nodemcu_xcon，基于python的控制台和测试脚本（运行在PC）。
nodemcu_lua，可以被上载到ESP8266的lua脚本（运行在SoC）。
