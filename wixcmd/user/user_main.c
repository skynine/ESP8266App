/*
*The wixServer based ESP8266 Soc.
*Author: openthings@163.com. 
*copyright & GPL V2.
*Last modified 2014-10-23.
*/

#include "ets_sys.h"
#include "xc.h"
#include "user_interface.h"
#include "osapi.h"
#include "driver/uart.h"


//*************************//
//系统初始化，由内核调用.
//建议一般不要进行修改.
//-------------------------//
void user_init(void)
{
	//串口初始化，可在此修改串口的波特率。	
	uart_init(BIT_RATE_9600, BIT_RATE_9600);
	
	//打印初始化信息。
	os_printf("\r\nsystem ready.\r\n");
	uart0_sendStr("\r\nwixServer ready.\r\n");

	//自己的应用的初始化入口.
	app_init();
}
//*************************//



