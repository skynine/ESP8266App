#ifndef __USER_PORT_H__
#define __USER_PORT_H__

#include "xc.h"
#include "user_interface.h"
#include "osapi.h"
#include "driver/uart.h"

/** @defgroup AT_PORT_Defines
  * @{
  */
#define at_cmdLenMax 128
#define at_dataLenMax 2048

#define app_busyTaskPrio        1
#define app_busyTaskQueueLen    4

#define app_procTaskPrio        0
#define app_procTaskQueueLen    1

static os_event_t	app_busyTaskQueue[app_busyTaskQueueLen];
static os_event_t	app_procTaskQueue[app_procTaskQueueLen];

//初始化。
void ICACHE_FLASH_ATTR 	app_init(void);

//创建的系统任务。
static void 		app_procTask(os_event_t *events);
static void 		app_busyTask(os_event_t *events);

//串口中断处理。
void 			uart_recvTask(uint8_t byteData);

//WiFi中断处理。
//void

#endif
