将生成的固件刷新到芯片中：

1、合并固件，使用app.make.sh，生成app.wixserver.bin。
2、烧写固件，模块重新加电，运行app.flash.sh，将app.wixserver.bin写入SoC中。
3、模块重新加电。运行minicom或其它的串口工具。
4、输入@，如果返回OK，则刷新成功。

在linux虚拟机中，拔出模块前先行去选端口。
否则下次连入端口号自动增加,导致无法连接到原来的端口号。
==================================================
app.make.sh，合并image为单个文件app.wixserver.bin。
app.flash.sh，烧写合并后的app.wixserver.bin。
app.install.sh，合并image为单个文件app.wixserver.bin并烧写。
commit.sh，提交所有更改到http://git.oschina.net/supergis/ESP8266App。

push2host.sh，
合并image并复制到共享的宿主机目录，供调试使用。
然后，在宿主机使用app.flash.sh可直接烧写最新的app.wixserver.bin。
