#!/bin/bash

echo "Upload firmware to ESP8266..." 
echo "Don't forget set GPIO0 to LOW."

echo "writing firmware/0x00000.bin"
#sudo python esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./app.wixserver.bin
sudo python esptool.py --port /dev/tty.wchusbserial1410 write_flash 0x00000 ./app.wixserver.bin

echo "Finished."
echo "Don't forget unset GPIO0 to HIGH."

