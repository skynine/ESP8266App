/*********************************************
 *UDPserver for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"

#include "version.h"

#include "espconn.h"
#include "user_json.h"
#include "driver/uart.h"
#include "../xflow/xc_cmd.h"

#include "wix_udpclient.h"

extern cmd_functionType cmd_fun[xc_cmdNum];

static struct espconn esp_conn_udpclient;
static uint16_t udpclient_port = 8267;

void udpclient_connect(char* udpserver, uint32 port)
{
	uint32_t ip = ipaddr_addr(udpserver);

	esp_conn_udpclient.type = ESPCONN_UDP;
	esp_conn_udpclient.state = ESPCONN_NONE;
	esp_conn_udpclient.proto.udp = (esp_udp *)os_zalloc(sizeof(esp_udp));
    esp_conn_udpclient.proto.udp->local_port = espconn_port();
    esp_conn_udpclient.proto.udp->remote_port = port;
    os_memcpy(esp_conn_udpclient.proto.udp->remote_ip, &ip, 4);
    esp_conn_udpclient.reverse = NULL;
	
    espconn_regist_recvcb(&esp_conn_udpclient, udpclient_recv);
    //espconn_regist_sentcb(esp_conn_udpclient, at_tcpclient_sent_cb);
	
    //if ((ip == 0xffffffff) && (os_memcmp(ipTemp,"255.255.255.255",16) != 0)) {
      //specialAtState = FALSE;
    //  espconn_gethostbyname(&esp_conn_udpclient, ipTemp, &host_ip, at_dns_found);
    //} else {
      if (espconn_create(&esp_conn_udpclient)==0) {
		sendInfoAll("OK\r");
      } else {
		  sendInfoAll("UDPCLIENT fail.\r");
      }
    //}
}

void ICACHE_FLASH_ATTR	 udpclient_recon(void *arg, sint8 err)
{
	
}

/*******************************************************
 * UDP数据接收的回调函数。
 * *****************************************************/
void ICACHE_FLASH_ATTR udpclient_recv(void *arg, char *pusrdata, unsigned short len)
{
	pusrdata[len] = '\0';
	xc_flow_onWifiReceiving(pusrdata);
}

//UDP数据发送。
void ICACHE_FLASH_ATTR	 udpclient_send(char *pbuf, unsigned short length)
{
	espconn_sent(&esp_conn_udpclient, pbuf, length);
	

}

/*******************************************************
 * UDP设置。作为客户端的当前连接信息。
  ******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CUDPCLIENTQ(uint8_t id, char *pParam)
{
	sendInfoAll("Unavailable.");
}

/*******************************************************
 * UDP设置。启动作为客户端的连接。
 *******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CUDPCLIENT(uint8_t id, char *pParam)
{
	char temp[32];
	char delims[] = ",";
	char *ipAddr =  strtok( NULL, delims );
	char *ipPort =  strtok( NULL, delims );
	uint32_t port = atoi(ipPort);

	os_sprintf(temp, "UDPCLIENT TO:%s:%d\r", ipAddr, port);
	sendInfoAll(temp);

	udpclient_connect(ipAddr,port);	
}

/*******************************************************
 * UDP设置。关闭作为客户端的连接。
 *******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CUDPCLOSE(uint8_t id, char *pParam)
{
	if (espconn_delete(&esp_conn_udpclient)==ESPCONN_OK) {
		sendInfoAll("UDPCLIENT closed.");
		return;
	}
	sendInfoAll("UDPCLIENT CLOSE fail.");
}
