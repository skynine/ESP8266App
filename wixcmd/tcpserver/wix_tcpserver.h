/*********************************************
 *TCPServer for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __WIX_TCPSERVER_H__
#define __WIX_TCPSERVER_H__

//#define SERVER_PORT 8000
#define SERVER_SSL_PORT 443

uint16_t get_tcpserver(void);

//查询TCP/IP服务器。
void ICACHE_FLASH_ATTR xc_cmd_CIPSERVERQ(uint8_t id, char *pParam);
//启动TCP/IP服务器。
void ICACHE_FLASH_ATTR xc_cmd_CIPSERVER(uint8_t id, char *pParam);
//关闭TCP/IP服务器。
void ICACHE_FLASH_ATTR xc_cmd_CIPSCLOSE(uint8_t id, char *pParam);

void ICACHE_FLASH_ATTR		tcpserver_start();
void ICACHE_FLASH_ATTR		tcpserver_init(uint32 port);
void ICACHE_FLASH_ATTR		tcpserver_listen(void *arg);
void ICACHE_FLASH_ATTR		tcpserver_recv(void *arg, char *pusrdata, unsigned short length);
void ICACHE_FLASH_ATTR		tcpserver_send(char *pbuf, unsigned short length);
void ICACHE_FLASH_ATTR	 	tcpserver_recon(void *arg, sint8 err);
void ICACHE_FLASH_ATTR	 	tcpserver_discon(void *arg);


#endif
