/*********************************************
 *UDPserver for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#include "ets_sys.h"
#include "os_type.h"
#include "osapi.h"
#include "mem.h"
#include "user_interface.h"

#include "version.h"

#include "espconn.h"
#include "user_json.h"
#include "driver/uart.h"
#include "../xflow/xc_cmd.h"

#include "wix_udpserver.h"

extern cmd_functionType cmd_fun[xc_cmdNum];

//UDP全局连接句柄。
static struct espconn esp_conn_udpserver;
static uint16_t udpserver_port = 8266;

/////////////////////////////////////////////////////////////////////////////////
/*******************************************************
 * UDPServer。
 * *****************************************************/
void ICACHE_FLASH_ATTR udpserver_start()
{
	udpserver_init(udpserver_port);
}

void ICACHE_FLASH_ATTR	 udpserver_init(uint32 port)
{
	udpserver_port = port;
	esp_conn_udpserver.type = ESPCONN_UDP;
    esp_conn_udpserver.state = ESPCONN_NONE;
    esp_conn_udpserver.proto.udp = (esp_udp *)os_zalloc(sizeof(esp_udp));
	if (esp_conn_udpserver.proto.udp==NULL) {
		sendInfoAll("Memory fail.");
		return;
	}
    esp_conn_udpserver.proto.udp->local_port = udpserver_port;
    esp_conn_udpserver.reverse = NULL;
    espconn_regist_recvcb(&esp_conn_udpserver, udpserver_recv);
    //espconn_regist_sendcb(&esp_conn_udpserver, udp_recv);

	//espconn_accept(&esp_conn_udpserver);
    if (espconn_create(&esp_conn_udpserver)!=0) {
		sendInfoAll("UDPSERVER fail..");
	}
}


/*******************************************************
 * UDP数据接收的回调函数。
 * *****************************************************/
void ICACHE_FLASH_ATTR udpserver_recv(void *arg, char *pusrdata, unsigned short len)
{
	pusrdata[len] = '\0';
	xc_flow_onWifiReceiving(pusrdata);
}

//UDP数据发送。
void ICACHE_FLASH_ATTR	 udpserver_send(char *pbuf, unsigned short length)
{
	espconn_sent(&esp_conn_udpserver, pbuf, length);
}


/*******************************************************
 * UDP设置。查询服务器设置和状态。
 *******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CUDPSERVERQ(uint8_t id, char *pParam)
{
	char temp[32];
	os_sprintf(temp, "%s:%d\r", cmd_fun[id].cmdName, udpserver_port);
	sendInfoAll(temp);		
}

/*******************************************************
 * UDP设置。启动服务器。
 *******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CUDPSERVER(uint8_t id, char *pParam)
{
	char temp[32];	
	if (pParam!=NULL) {
		udpserver_port = atoi(pParam);
	}
	os_sprintf(temp, "%s:%d\r", cmd_fun[id].cmdName, udpserver_port);
	sendInfoAll(temp);
	udpserver_start();
}

/*******************************************************
 * UDP设置。关闭服务器。
 *******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CUDPSCLOSE(uint8_t id, char *pParam)
{
	if (espconn_delete(&esp_conn_udpserver)==ESPCONN_OK) {
		sendInfoAll("UDPSERVER closed.");
		return;
	}
	sendInfoAll("UDPSERVER CLOSE fail.");
}


