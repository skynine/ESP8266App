#ifndef __XC_VERSION_H__
#define __XC_VERSION_H__

#define XC_VERSION_main   0x00
#define XC_VERSION_sub    0x18

#define XC_VERSION   (XC_VERSION_main << 8 | XC_VERSION_sub)

#endif
