/*
 *WiFi and TCP/IP over WiFi library for ESP8266.
 *Author: openthings@163.com. copyright&GPL V2.
 *Last modified 2014-10-20.
 */
 
#ifndef __XC_IP_H
#define __XC_IP_H

#define at_linkMax 5

typedef enum
{
  teClient,
  teServer
}teType;

typedef struct
{
	BOOL linkEn;
  	BOOL teToff;
	uint8_t linkId;
	teType teType;
	uint8_t repeaTime;
	struct espconn *pCon;
}at_linkConType;

#endif
