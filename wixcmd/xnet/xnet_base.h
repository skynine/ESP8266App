#ifndef __XNET_BASE_H__
#define __XNET_BASE_H__

/* NOTICE---this is for 512KB spi flash.
 * you can change to other sector if you use other size spi flash. */
#define ESP_PARAM_START_SEC		0x3C

#define ESP_PARAM_SAVE_0    1
#define ESP_PARAM_SAVE_1    2
#define ESP_PARAM_FLAG      3

#define packet_size   (2 * 1024)

#define token_size 41

struct esp_platform_saved_param {
    uint8 devkey[40];
    uint8 token[40];
    uint8 activeflag;
    uint8 pad[3];
};

struct esp_platform_sec_flag_param {
    uint8 flag; 
    uint8 pad[3];
};

enum {
    DEVICE_CONNECTING = 40,
    DEVICE_ACTIVE_DONE,
    DEVICE_ACTIVE_FAIL,
    DEVICE_CONNECT_SERVER_FAIL
};

//
void ICACHE_FLASH_ATTR 	xnet_load_param(struct esp_platform_saved_param *param);
void ICACHE_FLASH_ATTR 	xnet_save_param(struct esp_platform_saved_param *param);
void ICACHE_FLASH_ATTR 	xnet_get_token(uint8_t *token);
void ICACHE_FLASH_ATTR 	xnet_set_token(uint8_t *token);
void ICACHE_FLASH_ATTR 	xnet_set_active(uint8 activeflag);
void ICACHE_FLASH_ATTR 	xnet_set_connect_status(uint8 status);
int ICACHE_FLASH_ATTR	xnet_parse_nonce(char *pbuffer);
void ICACHE_FLASH_ATTR	xnet_get_info(struct espconn *pconn, uint8 *pbuffer);
void ICACHE_FLASH_ATTR	xnet_set_info(struct espconn *pconn, uint8 *pbuffer);

//Networking...
LOCAL void ICACHE_FLASH_ATTR	xnet_ap_change(void);
LOCAL bool ICACHE_FLASH_ATTR	xnet_reset_mode(void);

LOCAL void ICACHE_FLASH_ATTR	xnet_connect(struct espconn *pespconn);
LOCAL void ICACHE_FLASH_ATTR	xnet_reconnect(struct espconn *pespconn);
LOCAL void ICACHE_FLASH_ATTR	xnet_discon(struct espconn *pespconn);
LOCAL void ICACHE_FLASH_ATTR	xnet_sent(struct espconn *pespconn);
LOCAL void ICACHE_FLASH_ATTR	xnet_sent_beacon(struct espconn *pespconn);

//Callback of networking. 
LOCAL void ICACHE_FLASH_ATTR	xnet_connect_cb(void *arg);
LOCAL void ICACHE_FLASH_ATTR	xnet_recon_cb(void *arg, sint8 err);
LOCAL void ICACHE_FLASH_ATTR	xnet_sent_cb(void *arg);
LOCAL void ICACHE_FLASH_ATTR	xnet_recv_cb(void *arg, char *pusrdata, unsigned short length);
LOCAL void ICACHE_FLASH_ATTR	xnet_discon_cb(void *arg);

LOCAL void ICACHE_FLASH_ATTR	user_platform_rpc_set_rsp(struct espconn *pespconn, int nonce);
LOCAL void ICACHE_FLASH_ATTR	user_platform_timer_get(struct espconn *pespconn);
//LOCAL void ICACHE_FLASH_ATTR	xnet_upgrade_rsp(void *arg);
//LOCAL void ICACHE_FLASH_ATTR	xnet_upgrade_begin(struct espconn *pespconn, struct upgrade_server_info *server);

//DNS function.
LOCAL void ICACHE_FLASH_ATTR	xnet_dns_found(const char *name, ip_addr_t *ipaddr, void *arg);
LOCAL void ICACHE_FLASH_ATTR	xnet_dns_check_cb(void *arg);
LOCAL void ICACHE_FLASH_ATTR	xnet_start_dns(struct espconn *pespconn);
void ICACHE_FLASH_ATTR		xnet_check_ip(uint8 reset_flag);
void ICACHE_FLASH_ATTR		xnet_init(void);


#endif
