#ifndef __XNET_TIMER_H__
#define __XNET_TIMER_H__

struct xnet_wait_timer_param {
    uint8 wait_time_param[11];
    uint8 wait_action[15];
    int wait_time_second;
};

struct wait_param {
    uint8 action[20][15];
    uint16 action_number;
    uint16 count;
    uint32 min_time_backup;
};


uint16 ICACHE_FLASH_ATTR	split(char *p1, char *p2, char *splits[]);
int ICACHE_FLASH_ATTR		indexof(char *p1, char *p2, int start);

//void ICACHE_FLASH_ATTR	xnet_timer_start(char* pbuffer, struct espconn *pespconn);
void ICACHE_FLASH_ATTR	xnet_find_min_time(struct xnet_wait_timer_param *timer_wait_param , uint16 count);
void ICACHE_FLASH_ATTR	xnet_timer_first_start(uint16 count);
void ICACHE_FLASH_ATTR	xnet_device_action(struct wait_param *pwait_action);

void ICACHE_FLASH_ATTR	xnet_wait_time_overflow_check(struct wait_param *pwait_action);
void ICACHE_FLASH_ATTR	xnet_timer_action(struct xnet_wait_timer_param *timer_wait_param, uint16 count);
void ICACHE_FLASH_ATTR	xnet_timer_start(char *pbuffer);

#endif
