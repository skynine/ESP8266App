/*********************************************
 *WiFi configration for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __XC_CMD_WIFI_H__
#define __XC_CMD_WIFI_H__

//无线运行模式查询。
void xc_cmd_CWMODEQ(uint8_t id, char* pParam);
//无线运行模式设置。
void xc_cmd_CWMODE(uint8_t id, char* pParam);

//无线运行频道查询。
void xc_cmd_CWCHANNELQ(uint8_t id, char* pParam);
//无线运行频道设置。
void xc_cmd_CWCHANNEL(uint8_t id, char* pParam);

//扫描无线热点，返回列表。
void xc_cmd_CWLAP(uint8_t id, char* pParam);
static void ICACHE_FLASH_ATTR scan_done(void *arg, STATUS status);

void xc_connectAP(char* ap_ssid,char* ap_pwd);

//查询无线接入点配置参数。
void xc_cmd_CWJAPQ(uint8_t id, char* pParam);
//配置无线接入点。
void xc_cmd_CWJAP(uint8_t id, char* pParam);

//连接到无线接入点。
void xc_cmd_CWCAP(uint8_t id, char* pParam);
//退出无线接入点连接。
void xc_cmd_CWQAP(uint8_t id, char* pParam);

//软AP参数查询。
void xc_cmd_CWSAPQ(uint8_t id, char* pParam);
//软AP参数设置。
void xc_cmd_CWSAP(uint8_t id, char* pParam);
//软AP的当前接入设备列表。
void xc_cmd_CWLIF(uint8_t id, char* pParam);

#endif