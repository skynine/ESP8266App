#!/bin/bash

export PATH=/home/supermap/OpenThings/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
echo $PATH

echo "================================="
echo "pull all for ESP8266."
echo "================================="

echo "1-pull ESP8266App."
git pull

echo "2-pull esp-open-sdk."
cd esp-open-sdk
git pull
cd ..

echo "3-pull esp-mqtt."
cd esp_mqtt
git pull
cd ..

echo "4-pull esp_iot_rtos_sdk."
cd espressif/esp_iot_rtos_sdk
git pull
cd ../../

echo "5-pull esp8266_at."
cd espressif/esp8266_at
git pull
cd ../../

echo "6-pull micropython for esp8266."
cd micropython
git pull
cd ..

echo "7-pull nodemcu."
cd nodemcu/nodemcu-firmware
git pull
cd ../../

echo "8-pull nodelua."
cd nodelua
git pull
cd ..

echo "9-pull frankenstein."
cd es8266-frankenstein
git pull
cd ..

