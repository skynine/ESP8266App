#!/bin/bash

export PATH=/home/supermap/OpenThings/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
echo $PATH

echo "================================="
echo "Build all for ESP8266."
echo "================================="

echo "1-build ESP8266App."
cd ESP8266App
make
cd ..

echo "2-build esp-open-sdk."
cd esp-open-sdk
make
cd ..

echo "3-build rtos of esp8266."
cd espressif/esp_iot_rtos_sdk
make
cd ../../

echo "4-build micropython for esp8266."
cd micropython/esp8266
make
cd ../../

echo "5-build nodemcu-firmware."
cd nodemcu-firmware
make
cd ..

echo "6-build nodelua."
cd nodelua
make
cd ..


