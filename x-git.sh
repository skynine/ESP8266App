#!/bin/bash

export PATH=/home/supermap/OpenThings/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
echo $PATH

echo "================================="
echo "git all for ESP8266."
echo "================================="

echo "1-Please git ESP8266App on parent, run as: "
echo "git clone https://git.oschina.net/supergis/ESP8266App"

echo "2-git esp-open-sdk."
git clone https://github.com/pfalcon/esp-open-sdk.git

echo "3-git esp-mqtt."
git clone https://github.com/tuanpmt/esp_mqtt

echo "4-git rtos-espressif of esp8266."
mkdir espressif
cd espressif
git clone https://github.com/espressif/esp_iot_rtos_sdk.git

echo "5-git at-espressif of esp8266."
git clone https://github.com/espressif/esp8266_at.git
cd ..

echo "6-git micropython for esp8266."
git clone https://github.com/micropython/micropython.git

echo "7-git nodemcu-lua for esp8266."
mkdir nodemcu
cd nodemcu
git clone https://github.com/nodemcu/nodemcu-firmware.git
cd ..

echo "8-git nodelua."
git clone https://github.com/haroldmars/nodelua.git

echo "9-git frankenstein."
git clone https://github.com/supergis/esp8266-frankenstein.git

echo "Git clone for ESP8266, all ok."

