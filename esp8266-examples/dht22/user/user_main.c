/*
	Example read temperature and humidity from DHT22
	DHT22 connected to GPIO2

*/

#include <ets_sys.h>
#include <osapi.h>
#include <os_type.h>
#include <gpio.h>
#include "driver/uart.h"
#include "driver/dht22.h"

#define DELAY 2000 /* milliseconds */

LOCAL os_timer_t dht22_timer;
extern int ets_uart_printf(const char *fmt, ...);
int (*console_printf)(const char *fmt, ...) = ets_uart_printf;

LOCAL void ICACHE_FLASH_ATTR dht22_cb(void *arg)
{
	float * r = DHTRead();
	console_printf("Temperature: %d *C, Humidity: %d %%\r\n", (int)(r[0]*100), (int)(r[1]*100));
}

void user_init(void)
{
	uart_init(BIT_RATE_9600,BIT_RATE_9600);
	DHTInit();
	os_timer_disarm(&dht22_timer);
	os_timer_setfn(&dht22_timer, (os_timer_func_t *)dht22_cb, (void *)0);
	os_timer_arm(&dht22_timer, DELAY, 1);
}
