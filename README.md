#ESP8266App

<p>This project is a app collections of ESP8266 WiFi SoC.
</p>
Include:<p>
1.App as fireware using SDK.<br>
but not include SDK,which provide by www.espressif.com.<br>
2.Arduino app using ESP8266Lib,a opensource lib.<br>
At https://git.oschina.net/supergis/ESP8266Lib<br>
3.Other MCU app based esp8266 module.<br>
4.Apps for Android/iOS/Linux/Windows.<br>
5.Utilies for test and flash IC.<br>

<p>2014-12-30:
<br>Add more Interesting project for esp8266 with opensource.
<br>include: **nodemcu,nodelua,mpython,rtos,at,mqtt,frankstein**.
<br>can using x-git clone this project quickly.
<br>latest build placed at ./firmware.

<p>2014-12-28:
<br>Add a esp8266 resource list, on **./resources/esp8266_resources.txt**.

<p>2014-12-26:
<br>Add esp8266-examples and wixcmd all sourcecode. wixcmd implement XCMD protocol,which transmit data with Arduino or other MCU smartly.
<br>XCMD like AT commandset,but with "@+" head other than "AT+".
<br>If data-string not begin with "@",then transmit data between uart and wifi Immediately, which fast and simple than any other protocol.
